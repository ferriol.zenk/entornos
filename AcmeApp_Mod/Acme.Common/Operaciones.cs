﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acme.Common
{
    /// <summary>
    /// Clase estatica para los metodos de calculo
    /// </summary>
    public static class Operaciones
    {
        /// <summary>
        /// Metodo para sumar dos valores
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double suma(double a, double b)
        {
            return a + b;
        }
        /// <summary>
        /// Metodo para restar dos valores
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double resta(double a, double b)
        {
            if (a > b)
            {
                return a - b;
            }
            else
            {
                return b - a;
            }
        }
        /// <summary>
        /// Metodo para mutiplicar dos valores
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double multiplicacion(double a, double b)
        {
            return a * b;
        }
        /// <summary>
        /// Metodo para calcular la pontencia, pasandole potencia y valor
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double potencia(double a, int b)
        {
            return Math.Pow(a, b);
        }
    }
}
