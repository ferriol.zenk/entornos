﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acme.Biz;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.Biz.Tests
{
    [TestClass()]
    public class CalculadoraTest
    {
        [TestMethod()]
        public void ResultadoTest()
        {
            //Arrange
            Calculadora calc = new Calculadora();
            double resultado = 10;
            var expected = "El resultado es: 10";//Resultado esperado
            //Act
            var actual = calc.Resultado(resultado);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void MostrarInforTest()
        {
            //Arrange
            Calculadora calc = new Calculadora(1,2);
            var expected = string.Format("El primer numero es: {0}\nEl segundo numero es: {1}", 1, 2);//Resultado esperado
            //Act
            var actual = calc.MostrarInfo();

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}