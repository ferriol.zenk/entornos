﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.Common.Tests
{
    [TestClass()]
    public class OperacionesTest
    {
        [TestMethod]
        public void sumaTest()
        {
            //Arrange
            int solucion = 20; //Resultado esperado
            //Act
            double sumaTest = Operaciones.suma(10, 10);
            //Assert
            Assert.AreEqual(solucion, sumaTest);
        }

        [TestMethod]
        public void restaTest()
        {
            //Arrange
            int solucion = 5;//Resultado esperado
            //Act
            double restaTest = Operaciones.resta(10, 5);
            //Assert
            Assert.AreEqual(solucion, restaTest);

        }

        [TestMethod]
        public void multiplicacionTest()
        {
            //Arrange
            int solucion = 0;//Resultado no esperado
            //Act
            double multiTest = Operaciones.multiplicacion(5, 5);
            //Assert
            Assert.AreNotEqual(solucion, multiTest);
        }

        [TestMethod]
        public void potenciaTest()
        {
            //Arrange
            int solucion = 0;//Resultado no esperado
            //Act
            double potenciaTest = Operaciones.multiplicacion(5, 2);
            //Assert
            Assert.AreNotEqual(solucion, potenciaTest);
        }
    }
}