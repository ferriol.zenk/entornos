﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.Biz
{
    /// <summary>
    /// Clase calculadora
    /// </summary>
    public class Calculadora
    {
        #region Atributos y Propiedades
        /// <summary>
        /// Atributos
        /// </summary>
        private double num1;
        private double num2;

        /// <summary>
        /// Propiedades
        /// </summary>
        public double Num1 { get { return num1; } set { num2 = value; } }
        public double Num2 { get { return num1; } set { num2 = value; } }
        #endregion

        #region Contructores
        /// <summary>
        /// Constructores
        /// </summary>
        public Calculadora()
        {
            Console.WriteLine("Instancia de la calculadora creada");
        }
        /// <summary>
        /// Constructor sobrecargado
        /// </summary>
        /// <param name="n1"></param>
        /// <param name="n2"></param>
        public Calculadora(double n1, double n2)
        {
            num1 = n1;
            num2 = n2;
        }
        #endregion

        #region Metodos de Interfaz
        /// <summary>
        /// Muestra el resultado que se le pase por parametro
        /// </summary>
        /// <param name="resultado"></param>
        public string  Resultado(double resultado)
        {
            return string.Format("El resultado es: {0}", resultado);
        }
        /// <summary>
        /// Muestra los numeros de los atributos
        /// </summary>
        public string MostrarInfo()
        {
            return string.Format("El primer numero es: {0}\nEl segundo numero es: {1}", num1, num2);
        }
        #endregion

    }
}
